# Internal DDoS
## What is it?
- Malicious hosts and victim live on the same network
- Attacker uses the malicious devices to send a DoS or DDoS attack to the victim
- Typically less of a focus to no focus at all on DDoS protection and mitigation for LAN traffic

## How it works
With this approach, the devices are typically servers either bruteforced or purchased by the attacker. From there, the attacker infects them all with software that will connect back to a botnet Command and Control (CNC) server.
After all of the devices have been configured for DDoS usage, the attacker will launch an attack from the CNC server. It works in the same way as a traditional botnet attack, except the slaves are not generic devices; they are hand-picked in order to bypass mitigation efforts on external traffic.
Whereas DDoS mitigation efforts have concentrated on how to properly manage external traffic that is arriving, even large hosting providers do not have systems put in place to stop floods from traversing other devices on the same network.

![enter image description here](https://gitlab.com/cryptio/ddos-research/-/raw/master/images/internal-ddos.png)

## Effectiveness
As previously mentioned, not a lot of hosting providers or infrastructures focus on the possibility of internal floods between LAN devices.

### Examples
#### OVH
To quote [the following page](https://docs.ovh.com/ca/en/dedicated/firewall-network/):

> The Firewall Network is not taken into account within the OVH network,
> so the rules set up do not affect the connections in this internal
> network.

Much like the rest of OVH's anti-DDoS system, Armor (their configurable network firewall) does not apply to internal traffic. This means that DDoS attacks originating from other OVH servers will not pass through VAC, which becomes problematic if you have people with a few servers that are using all of their bandwidth to flood yours.

## Mitigation
### Rate limiting inbound LAN traffic
If you are convinced that it is unnecessary or far less common for your system to communicate with other devices on the same network, you may rate limit traffic originating from inside of the LAN. You should make sure that this will not impact the performance of your services.

### Blocking and reporting the offending servers
If you manage to get a packet capture of the DDoS attack, you can block the offending IP addresses using your system's firewall. It is recommended to also report these IP addresses to the hosting provider. If they are a sensible company, then the attacker and his services will be terminated, thus causing them to lose money and the ability to DDoS others on this network.

### Patching the DDoS attack used
Another way to help mitigate the impact of internal attacks is by dropping the type of attack used. You may be able to examine the traffic and deduce an anomaly in what is being sent, determining that it will not block out legitimate traffic and is therefore not required and safe to drop.

### Raising these concerns to the hosting providers
If enough attention is brought to network administrators, certain restrictions could be implemented to prevent these host-to-host floods that happen in local networks.