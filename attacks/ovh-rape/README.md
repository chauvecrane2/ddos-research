# OVH-RAPE
## History
### First sighting
February 23rd, 2020 - Attacker under the alias "Sennheiser" went around flexing an unrecognized method on servers hosted by OVH

### Mainstreaming
March 5th, 2020 - Method was sold to SecurityTeam.io, a popular DDoS-for-hire service, and marketed under the name of "UDP-RAPE".
Shortly after, a "UDP-RAPE v2" method came out which was the exact same program except set the source port to that of services which were commonly used for reflection (i.e 1194 for OpenVPN, 27036 for VSE, 3478 for STUN over UDP).

May 6th, 2020 - UDP-RAPE modified by the alias "ungv" was added to a botnet and used against VPN services. The only difference is that the payload is the ASCII character "7" repeated many times. The payload length is still randomized.

May 14th, 2020 - Method called "R6" discovered on Supreme Security Team, another DDoS for hire service. Modified version of OVH-RAPE that has a fixed-length payload of 52 bytes.

June 1st, 2020 - Method used on a Discord DDoS bot called "ShadowSec", referred to as "OVH-PLUS".

### Leak
April 15th, 2020 - User MIRAIP0TS on Pastebin released the [source code](https://pastebin.com/bfWeQrwk)

### OVH informed about the issue
March 3rd, 2020 - I reached out to OVH through a support ticket, explaining that this method was exploiting how their Anti-DDoS system whitelisted traffic from specific services to bypass VAC:

> Hi, 
> I am sure this type of request is "not covered by my support
> level", so I am not writing this to ask anything but rather to inform.
> There is a type of DDoS attack circulating around referred to by many
> names, some being "OVH RAPE",  "UDP RAPE", and "UDP RAPE v2".  From
> what I have observed, this attack leverages IPHM to mask itself as
> traffic coming from Cloudflare. I have heard that it's possible that
> VAC has Cloudflare IP ranges on a whitelist. This appears to cause the
> DDoS traffic to pass through OVH's infrastructure without setting off
> any red flags. The instances of this attack that I've seen only have
> about 3-5 Gbps of throughput, but since it is not being vacuumed up,
> it is enough to completely saturate the resources of any targeted
> server. The other attributes of it are nothing special: it uses a
> randomised payload and increments the destination port for every
> packet sent. A possible solution could be heavily rate limiting UDP
> traffic from Cloudflare that isn't DNS. This type of DDoS attack is
> becoming more prevalent as it makes its way into the hands of 
> DDoS-for-hire services, and it is causing distress for many clients. I
> believe that it would be worth investigating, as OVH's capacity
> against DDoS attacks is in my opinion one of the highlights of using
> them. However, as this gets abused more and more, the people who share
> the same thoughts as myself will move to hosting providers who
> actively respond to any attack that uncovers a flaw in their system
> regardless of how much they pay for support. I do not believe that any
> other host offering DDoS protection is affected by such an attack,
> including companies like NFO, Path Network, Psychz Networks, and more.
> It would be beneficial to the entire clientbase. If captures are
> necessary, I can provide them.

After many more email exchanges, OVH informed me that they would look into this on March 10th.

March 27th, 2020 - We finally observed VAC heavily filtering OVH-RAPE on VPS SSD products, with the peak being 30Mbps before it was quickly mitigated and reduced to nothing. It appears as if OVH Game servers still get affected by this, which may be due to the Anti-DDoS Game firewall.

## How it works
- OVH whitelists certain traffic that is commonly contacted, such as Cloudflare, Steam, Google, and Microsoft IP ranges, as well as different nameservers. This traffic does not get filtered the same way that other traffic does. VAC will only kick in to rate limit the traffic if too many packets per second (PPS) are sent from the source address, but this is unlikely since the original program uses 432 different IP addresses
- OVH-RAPE uses IP Header Modification (IPHM) to iterate over a list of the whitelisted IP addresses. It generates UDP packets with a pseudo-random payload and source port, then it sets the source IP address to one of these whitelisted IPs and sends the attack. One or more servers that are able to push at least a few gigabits per second (Gbps) will be capable of saturating almost any client's server on OVH's network

## Mitigation
As of March 27th, 2020, it appears as if OVH VPS products have this attack patched, while OVH Game servers still struggle. Please do not use the below solutions unless you are still being severely affected.

### Blocking the packet based on the payload
While the payload and packet length is pseudo-random, most of the traffic has the following hexadecimal data:
    `fe fe fe fe fe`
You could rate limit such traffic like this, just to be safe that you don't block any legitimate traffic containing that:
```bash
iptables -N ovh_rape
iptables -A INPUT -p udp -m string --algo kmp --hex-string "|fe fe fe fe fe|" --from 28 -j ovh_rape  
iptables -A ovh_rape -m limit --limit 100/s --limit-burst 500 -j RETURN  
iptables -A ovh_rape -j DROP
```


### Limiting non-DNS UDP Cloudflare traffic 
Since the program randomizes ports, it is unlikely that any of it will come from port 53. Using [this script I wrote](./ovh-rape-count.py), we can convert all of the IP addresses stored as decimals in the OVH-RAPE program into string representations. From there, we can check each IP address using IPInfo.io's API to find out which organization it belongs to. The script will tell us how many OVH-RAPE IP addresses belong to each organization:
```json
{
   "AS13335 Cloudflare, Inc.\n":126,
   "AS32590 Valve Corporation\n":24,
   "AS8075 Microsoft Corporation\n":19,
   "AS15169 Google LLC\n":19,
   "AS24940 Hetzner Online GmbH\n":9,
   "AS12876 ONLINE S.A.S.\n":8,
   "AS197000 Reseaux IP Europeens Network Coordination Centre (RIPE NCC)\n":7,
   "AS397241 NeuStar, Inc.\n":6,
   "AS12041 Afilias, Inc.\n":6,
   "AS42 WoodyNet\n":6,
   "AS32934 Facebook, Inc.\n":6,
   "AS54113 Fastly\n":5,
   "AS63949 Linode, LLC\n":5,
   "AS14061 DigitalOcean, LLC\n":5,
   "AS16509 Amazon.com, Inc.\n":5,
   "AS208294 Markus Koch\n":5,
   "AS2635 Automattic, Inc\n":5,
   "AS40717 VeriSign Global Registry Services\n":5,
   "AS8068 Microsoft Corporation\n":4,
   "AS20446 Highwinds Network Group, Inc.\n":4,
   "AS12322 Free SAS\n":4,
   "AS13414 Twitter Inc.\n":4,
   "AS397210 VeriSign Global Registry Services\n":4,
   "AS174 Cogent Communications\n":3,
   "AS10439 CariNet, Inc.\n":3,
   "AS8560 1&1 IONOS SE\n":3,
   "AS49981 WorldStream B.V.\n":3,
   "AS397209 VeriSign Global Registry Services\n":3,
   "AS397204 VeriSign Global Registry Services\n":3,
   "AS3320 Deutsche Telekom AG\n":3,
   "AS28001 LACNIC - Latin American and Caribbean IP address\n":3,
   "AS33517 Oracle Corporation\n":3,
   "AS4134 CHINANET-BACKBONE\n":2,
   "AS396982 Google LLC\n":2,
   "AS202425 IP Volume inc\n":2,
   "AS29182 JSC The First\n":2,
   "AS34010 Yahoo! UK Services Limited\n":2,
   "AS15133 MCI Communications Services, Inc. d/b/a Verizon Business\n":2,
   "AS45090 Shenzhen Tencent Computer Systems Company Limited\n":2,
   "AS134771 WENZHOU, ZHEJIANG Province, P.R.China.\n":2,
   "AS36459 GitHub, Inc.\n":2,
   "AS17974 PT Telekomunikasi Indonesia\n":2,
   "AS40647 VeriSign Global Registry Services\n":2,
   "AS53535 ARIN Operations\n":2,
   "AS393225 ARIN Operations\n":2,
   "AS28573 CLARO S.A.\n":2,
   "AS49505 OOO Network of data-centers Selectel\n":1,
   "AS206262 TelKos L.L.C\n":1,
   "AS45899 VNPT Corp\n":1,
   "AS134761 CHINANET Sichuan province Chengdu MAN network\n":1,
   "AS198347 Game-Hosting GH AB\n":1,
   "AS20473 Choopa, LLC\n":1,
   "AS133398 Tele Asia Limited\n":1,
   "AS9269 Hong Kong Broadband Network Ltd.\n":1,
   "AS51815 IP-Only Networks AB\n":1,
   "AS29802 HIVELOCITY, Inc.\n":1,
   "AS20431 VeriSign Global Registry Services\n":1,
   "AS22773 Cox Communications Inc.\n":1,
   "AS7922 Comcast Cable Communications, LLC\n":1,
   "AS20926 Oceanet Technology SAS\n":1,
   "AS5483 Magyar Telekom plc.\n":1,
   "AS21502 SFR SA\n":1,
   "AS50613 Advania Island ehf\n":1,
   "AS47541 VKontakte Ltd\n":1,
   "AS201912 FutureNow Incorporated\n":1,
   "AS45031 dogado GmbH\n":1,
   "AS6830 Liberty Global B.V.\n":1,
   "AS41231 Canonical Group Limited\n":1,
   "AS1200 Amsterdam Internet Exchange B.V.\n":1,
   "AS41064 Telefun SAS\n":1,
   "AS48119 Firma Handlowo - Uslugowa MultiFOX\n":1,
   "AS197422 Tetaneutral.net\n":1,
   "AS203476 GANDI SAS\n":1,
   "AS41853 Limited Liability Company NTCOM\n":1,
   "AS58474 PT. MATRIXNET GLOBAL INDONESIA\n":1,
   "AS43519 Nominet UK\n":1,
   "AS38365 Beijing Baidu Netcom Science and Technology Co., Ltd.\n":1,
   "AS132203 Tencent Building, Kejizhongyi Avenue\n":1,
   "AS133119 China Unicom IP network\n":1,
   "AS13 Headquarters, USAISC\n":1,
   "AS197071 active 1 GmbH\n":1,
   "AS1280 Internet Systems Consortium, Inc.\n":1,
   "AS397242 NeuStar, Inc.\n":1,
   "AS44273 Host Europe GmbH\n":1,
   "AS51167 Contabo GmbH\n":1,
   "AS60868 Runtime Collective Limited\n":1,
   "AS59796 Storm Systems LLC\n":1,
   "AS201206 Droptop GmbH\n":1,
   "AS31529 DENIC eG\n":1,
   "AS64439 IT Outsourcing LLC\n":1,
   "AS3557 Internet Systems Consortium, Inc.\n":1,
   "AS2149 Cogent Communications\n":1,
   "AS29216 NETNOD Internet Exchange i Sverige AB\n":1,
   "AS5927 DoD Network Information Center\n":1,
   "AS30148 Sucuri\n":1,
   "AS31863 Centrilogic, Inc.\n":1,
   "AS21556 NASA Ames Research Center\n":1,
   "AS137 Consortium GARR\n":1,
   "AS25192 CZ.NIC, z.s.p.o.\n":1,
   "AS48283 Stichting Internet Domeinregistratie Nederland\n":1,
   "AS199670 DNS-Belgium\n":1,
   "AS2200 Renater\n":1,
   "AS8674 NETNOD Internet Exchange i Sverige AB\n":1,
   "AS30889 Waycom International (SASU)\n":1,
   "AS42385 Joint-stock company Internet Exchange MSK-IX\n":1,
   "AS37181 African Network Information Center - ( AfriNIC Ltd. )\n":1,
   "AS32475 SingleHop LLC\n":1,
   "AS1508 Headquarters, USAISC\n":1,
   "AS237 Merit Network Inc.\n":1,
   "\n":1,
   "AS20144 ICANN\n":1,
   "AS10886 University of Maryland\n":1,
   "AS28504 Network Information Center México S.C.\n":1,
   "AS10906 Núcleo de Inf. e Coord. do Ponto BR - NIC.BR\n":1,
   "AS11284 Núcleo de Inf. e Coord. do Ponto BR - NIC.BR\n":1,
   "AS11644 Núcleo de Inf. e Coord. do Ponto BR - NIC.BR\n":1,
   "AS12136 Núcleo de Inf. e Coord. do Ponto BR - NIC.BR\n":1,
   "AS7500 WIDE Project\n":1,
   "AS18366 ANYCAST AS\n":1,
   "AS38265 Sisaket Rajabhat University\n":1,
   "AS134690 Chulachomklao Royal Military academy member of UniNet\n":1,
   "AS23600 Korea Internet Security Agency\n":1,
   "AS7713 PT Telekomunikasi Indonesia\n":1,
   "AS18368 APNIC DNS Anycast\n":1,
   "AS18369 APNIC ANYCAST\n":1,
   "AS10515 VeriSign Infrastructure & Operations\n":1,
   "AS3462 Data Communication Business Group\n":1,
   "AS12297 VEON Armenia CJSC\n":1,
   "AS29075 IELO-LIAZO SERVICES SAS\n":1,
   "AS44066 First Colo GmbH\n":1,
   "AS397201 VeriSign Global Registry Services\n":1,
   "AS8342 JSC RTComm.RU\n":1
}
```


It is obvious that most IP addresses belong to Cloudflare. Given this information, we may choose to store a list of Cloudflare IP ranges. When an incoming packet arrives, we check if it is UDP traffic originating from one of Cloudflare's IP addresses that does not have the source port 53. 
```bash
ipset -N cloudflare hash:net  
cloudflare_ranges=(  
    173.245.48.0/20  
    103.21.244.0/22  
    103.22.200.0/22  
    103.31.4.0/22  
    141.101.64.0/18  
    108.162.192.0/18  
    190.93.240.0/20  
    188.114.96.0/20  
    197.234.240.0/22  
    198.41.128.0/17  
    162.158.0.0/15  
    104.16.0.0/12  
    172.64.0.0/13  
    131.0.72.0/22  
)
for i in "${cloudflare_ranges[@]}"; do ipset -A cloudflare "$i"; done
# Block UDP traffic from Cloudflare IPs that aren't DNS requests  
iptables -t raw -A PREROUTING -p udp -m set --match-set cloudflare src ! --sport 53 -j DROP
```

### Limiting all traffic from OVH-RAPE IPs
Another approach is simply getting the most frequent IP ranges that are used by OVH-RAPE, and then rate limiting all of them. You can lookup the IP addresses online to find out what range each one belongs to:
```bash
ipset -N ovh_rape hash:net  
ovh_rape_ranges=(  
    # Valve  
    155.133.246.0/23  
    155.133.248.0/24  
    162.254.192.0/24  
    162.254.193.0/24  
      
    # Microsoft  
    13.107.14.0/24  
    52.112.0.0/14  
      
    # Google  
    216.58.215.0/24  
    216.239.36.0/24  
    67.199.248.0/24  
    66.249.64.0/20  
)
for i in "${ovh_rape_ranges[@]}"; do ipset -A ovh_rape "$i"; done
    
iptables -N ovh_rape  
      
iptables -A INPUT -p udp -m set --match-set ovh_rape src -m limit -j ovh_rape  
iptables -A ovh_rape -m limit --limit 1000/s -j RETURN  
iptables -A ovh_rape -j DROP
```


### Blocking the IP ranges in the Pre Firewall (OVH's configurable firewall)
![enter image description here](https://gitlab.com/cryptio/ddos-research/-/raw/master/images/ovh-rape-armor-rules.png)

Instead of rate limiting the IP addresses using the host's network firewall, you may block all UDP traffic from them using OVH's firewall. This is only the way to benefit from OVH's infrastructure when mitigating this attack. Unfortunately, this may reject legitimate traffic. Although you should not be receiving non-DNS Cloudflare UDP traffic, the same cannot be said for Valve, Microsoft, Google, and other ASNs it uses. Putting the right combination of rules in may block 50% or more of the attack. Unfortunately, OVH'S Pre Firewall only allows you to enter up to 19 rules, so use each one wisely!