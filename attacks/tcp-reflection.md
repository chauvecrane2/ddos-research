# TCP Reflection
## How it works
- Attacker sends forged SYN packet to a legitimate TCP server with the source address spoofed to the victim's IP address. The source port may also be crafted to determine what destination port the attack will be reflected on
- Spoofed SYN packet arrives at the server. It has no way of knowing that the packet's source IP address is actually who sent it
- Server responds to the TCP SYN with a SYN-ACK packet, and the traffic gets sent to the victim who never tried to open a connection in the first place
![TCP reflection](https://gitlab.com/cryptio/ddos-research/-/raw/master/images/tcp-reflection.png)

## Efficiency
- Puts stress on conntrack and other stateful firewall modules, as a lookup has to be done for each orphan SYN-ACK packet coming in
- Hard to mitigate, attack is legitimate traffic from legitimate servers
- Hosting providers such as OVH and NFO struggle to deal with TCP reflection, and end up endlessly routing legitimate SYN-ACK traffic through a loop. This prevents new legitimate conenctions from being established.

## Mitigation
### Rate limiting
The most common reflector for this kind of attack is HTTP and HTTPS servers. We could rate limit TCP traffic coming from port 80 and 443, but this could also impact legitimate traffic. The goal of DDoS mitigation is to avoid interruptions. If non-DDoS traffic is still not able to make it through, then we are not accomplishing anything.

### Stateful inspection
A stateful firewall can be used to check if the incomng SYN-ACK packet belongs to a previously sent SYN packet. An attack with a high packets per second (PPS) rate will strain the system's resources as it tries to keep up in checking all of the incoming packets against all of the entries.

### SYNPROXY
Similar to a stateful inspection approach, one may have a program that acts as a proxy for incoming TCP connections. Before passing the traffic to the server, this will check to see if the client is actually completing the three way handshake instead of simply sending a single packet out of the entire process.

### Stopping the routing of spoofed traffic
The long-term solution to TCP reflection attacks, among many others such as UDP amplification, is getting the cooperation of network operators worldwide to stop routing spoofed traffic.

## Research
[The State of TCP Reflection Attacks](https://cyware.com/news/the-state-of-tcp-reflection-attacks-2cadd82d)

[Threat Alert: TCP Amplification Attacks](https://blog.radware.com/security/2019/11/threat-alert-tcp-reflection-attacks/)

[SYN-ACK Flood Attack](https://www.corero.com/resource-hub/syn-ack-flood-attack/)

## Notes
- The attacker must be forging the packets from within a network that will route spoofed traffic. Most hosting providers do not allow IP Header Modification (IPHM), so offenders look to bulletproof hosting.
- Not all TCP reflection may be SYN-ACK packets. For instance, a potential reflector server that chooses to refuse the attacker's incoming request may send an RST packet. While the throughput is essentially the same since these packets are only carrying a TCP header, RST packets may be rate limited aggressively.
- As of around March 13th, 2020, Cloudflare began developing flowtrack for Magic Transit, which can be summed up as "conntrack that only keeps track of outbound connections". This update is supposed to target TCP attacks.