# SYNPROXY
## What is it?
- iptables target
- Helps mitigate SYN, SYN-ACK, and ACK floods
- Proxies TCP handshakes before handing them to the server

## How it works
- Client sends SYN packet to the server
- Firewall marks the packet as `UNTRACKED` which is given to SYNPROXY
- SYNPROXY responds to the client with a SYN-ACK packet (UNTRACKED) on behalf of the server
- Client responds with ACK packet (marked as `INVALID` or `UNTRACKED` in iptables) which is also given to SYNPROXY
- After the client is connected to SYNPROXY, SYNPROXY automatically initiates a 3-way handshake with the real server. The SYN packet is spoofed so that it appears as if the client is trying to connect.  The TCP SYN is marked as `NEW` in iptables and happens in the OUTPUT chain
- Server responds with SYN-ACK to the client
- SYNPROXY receives the SYN-ACK and responds to the server with an ACK. Now, the connection is marked as `ESTABLISHED`
- Connection is now established, traffic can flow as normal

![enter image description here](https://pub.kb.fortinet.com/Platform/Publishing/images/cgustave/33596/synproxy2.png)

## Configuration
From [Mitigate TCP SYN Flood Attacks with Red Hat Enterprise Linux 7 Beta](https://www.redhat.com/en/blog/mitigate-tcp-syn-flood-attacks-red-hat-enterprise-linux-7-beta):
```bash
export DEV=yourprimarynic
export PORT=yourporttobeprotected
```

Make sure connections that need protection don't create new conntrack entries for SYN packets:

```bash
iptables -t raw -I PREROUTING -i $DEV -p tcp -m tcp --syn --dport $PORT -j CT --notrack
```

Stop ACK packets from creating new connections:

```bash
sysctl -w net.netfilter.nf_conntrack_tcp_loose=0
```

Enable TCP timestamps for SYN cookies:
```bash
sysctl -w net.ipv4.tcp_timestamps=1
```

Catch UNTRACKED SYN packets and INVALID ACK packets, give them to SYNPROXY:

```bash
iptables -A INPUT -i $DEV -p tcp -m tcp --dport $PORT -m state --state INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460
```

Drop all packets with the INVALID state that were not validated by SYNPROXY:
```bash
iptables -A INPUT -m state --state INVALID -j DROP
```

## Specifications
- Available as of Linux kernel 3.13
- Requires conntrack

## Performance
| Attack | Max PPS |
|--|--|
| SYN flood | 2,869,824 PPS |
| SYN-ACK flood | 5,653,120 PPS |
| ACK flood | 4,948,480 PPS |

## Sources
[Mitigate TCP SYN Flood Attacks with Red Hat Enterprise Linux 7 Beta](https://www.redhat.com/en/blog/mitigate-tcp-syn-flood-attacks-red-hat-enterprise-linux-7-beta)

[DDoS protection Using Netfilter/iptables](https://people.netfilter.org/hawk/presentations/devconf2014/iptables-ddos-mitigation_JesperBrouer.pdf)

[Working with SYNPROXY](https://github.com/firehol/firehol/wiki/Working-with-SYNPROXY)

## Footnotes
- Don't need to use conntrack matching on any traffic that is handled by SYNPROXY. A proper setup will handle the handshake and drop all packets that are not ESTABLSHED, so you do not need to something like this:

    ```bash
    iptables -A INPUT -p tcp -m conntrack --ctstate ESTABLISHED --dport $PORT
    ```
- Should only be used on public TCP services that need protection fron these attacks. Passing a lot of un-necessary TCP traffic through SYNPROXY will have performance impacts
